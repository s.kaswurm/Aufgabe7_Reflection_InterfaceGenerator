import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.junit.Test;

/**
 * Dieser Test prueft ob das Interface der Hash-Map-Klasse
 * den selben Inhalt wie das File "IfHashMap.java" aufweist.
 * 
 *  Das dazugehoerige File "IfHashMap" enthaelt alle
 *  erzeugten Methoden und Parametertypen der Klasse HashMap.
 * 
 * @author Stephanie Kaswurm und Anna-Lena Klaus
 */

public class GeneratorTest {

	@Test
	public void test() throws ClassNotFoundException  {
		
		/* Ein neuer Generator wird hier erstellt, und ein Interface der Klasse
		 * HashMap wird generiert. Und dann der Inhalt des generierten Interfaces 
		 * mit dem testInterface verglichen.
		 */
		Generator generatorTest = new Generator();
		generatorTest.InterfaceGenerator(HashMap.class);
		
		try {
			String[] generiertesInterface = dateiLesen("IfHashMap.java");
			String[] testInterface = dateiLesen("src/test/java/IfHashMap.java");
			assertArrayEquals(generiertesInterface, testInterface);
		
		// Wenn das File nicht gefunden wurde, dann wird eine Fehlermeldung ausgegeben.
		} catch (FileNotFoundException e) {
			fail("Test fehlgeschlagen, Datei nicht gefunden.");
		}	
	}
	
		// Der Scanner scannt das File (Dateiname) und prüft Linie für Line.
	public String[] dateiLesen(String dateiname) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(dateiname));
		List<String> dateiinhalt = new ArrayList<>();
		while (scanner.hasNextLine()){
			dateiinhalt.add(scanner.nextLine());
		}
		scanner.close();
				
		return dateiinhalt.toArray(new String [dateiinhalt.size()]);
	}
	
	
}
