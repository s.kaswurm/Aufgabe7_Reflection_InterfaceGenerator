public interface IfHashMap {
    public java.lang.Object remove(java.lang.Object arg0);
    public boolean remove(java.lang.Object arg0, java.lang.Object arg1);
    public java.lang.Object get(java.lang.Object arg0);
    public java.lang.Object put(java.lang.Object arg0, java.lang.Object arg1);
    public java.util.Collection values();
    public java.lang.Object clone();
    public void clear();
    public boolean isEmpty();
    public boolean replace(java.lang.Object arg0, java.lang.Object arg1, java.lang.Object arg2);
    public java.lang.Object replace(java.lang.Object arg0, java.lang.Object arg1);
    public void replaceAll(java.util.function.BiFunction arg0);
    public int size();
    public java.util.Set entrySet();
    public void putAll(java.util.Map arg0);
    public java.lang.Object putIfAbsent(java.lang.Object arg0, java.lang.Object arg1);
    public java.util.Set keySet();
    public boolean containsValue(java.lang.Object arg0);
    public boolean containsKey(java.lang.Object arg0);
    public java.lang.Object getOrDefault(java.lang.Object arg0, java.lang.Object arg1);
    public void forEach(java.util.function.BiConsumer arg0);
    public java.lang.Object computeIfAbsent(java.lang.Object arg0, java.util.function.Function arg1);
    public java.lang.Object computeIfPresent(java.lang.Object arg0, java.util.function.BiFunction arg1);
    public java.lang.Object compute(java.lang.Object arg0, java.util.function.BiFunction arg1);
    public java.lang.Object merge(java.lang.Object arg0, java.lang.Object arg1, java.util.function.BiFunction arg2);
}
